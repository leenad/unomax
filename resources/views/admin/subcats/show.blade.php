@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Subcat  {{ $subcat->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/subcats') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_subcats', 'delete_subcats')
                         <a href="{{ url('/admin/subcats/' . $subcat->id . '/edit') }}" title="Edit Subcat"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/subcats' . '/' . $subcat->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Subcat" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $subcat->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $subcat->title }} </td></tr><tr><th> Status </th><td> {{ $subcat->status }} </td></tr><tr><th> Category Id </th><td> {{ $subcat->category_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
