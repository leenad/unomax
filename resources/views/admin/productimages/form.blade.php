 <label for="product_id" style="font-size:18px;">{{ 'Product Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="product_id" class="form-control" id="product_id" required>
          @foreach ($products as  $optionValue)
              <option value="{{ $optionValue->id }}"  {{ (isset($productimage->product_id) && $productimage->product_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->name }}</option>
          @endforeach
        </select>
        <!-- <input class="form-control" name="product_id" type="number" id="product_id" value="{{ isset($productimage->product_id) ? $productimage->product_id : ''}}" required> -->
     {!! $errors->first('product_id', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="color_id" style="font-size:18px;">{{ 'Color Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="color_id" class="form-control" id="color_id">
          @foreach ($color as  $optionValue)
              <option value="{{ $optionValue->id }}"  {{ (isset($productimage->color_id) && $productimage->color_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->name }}</option>
          @endforeach
        </select>
        <!-- <input class="form-control" name="color_id" type="number" id="color_id" value="{{ isset($productimage->color_id) ? $productimage->color_id : ''}}" required> -->
     {!! $errors->first('color_id', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="product_color_image" style="font-size:18px;">{{ 'Product Color Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="product_color_image" type="file" id="product_color_image" value="{{ isset($productimage->product_color_image) ? $productimage->product_color_image : ''}}">
     {!! $errors->first('product_color_image', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1":"Active","0":"Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($productimage->status) && $productimage->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
