@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Productimage  {{ $productimage->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/productimages') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_productimages', 'delete_productimages')
                         <a href="{{ url('/admin/productimages/' . $productimage->id . '/edit') }}" title="Edit Productimage"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/productimages' . '/' . $productimage->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Productimage" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $productimage->id }}</td>
                                    </tr>
                                    <tr><th> Product Name </th><td> {{ $productimage->product_name }} </td></tr>
                                    <tr><th> Color Name </th><td> {{ $productimage->color_name }} </td></tr>
                                    <tr><th> Product Color Image </th>
                                        <td ><img style="width: 200px" src="{{ asset('storage/'.$productimage->product_color_image)}}"> </td></tr>
                                    <tr><th> Status </th><td> {{ $productimage->status==1?'Active':'Disabled' }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
