 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($product->name) ? $product->name : ''}}" >
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="short_description" style="font-size:18px;">{{ 'Short Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="short_description" type="textarea" id="short_description" >{{ isset($product->short_description) ? $product->short_description : ''}}</textarea>
     {!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="tag_line" style="font-size:18px;">{{ 'Tag Line' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="tag_line" type="text" id="tag_line" value="{{ isset($product->tag_line) ? $product->tag_line : ''}}" >
     {!! $errors->first('tag_line', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="category_id" style="font-size:18px;">{{ 'Category Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="category_id" class="form-control" id="category_id" >
          @foreach ($category as  $optionValue)
              <option value="{{ $optionValue->id }}"  {{ (isset($product->category_id) && $product->category_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->name }}</option>
          @endforeach
        </select>
        <!-- <input class="form-control" name="category_id" type="number" id="category_id" value="{{ isset($product->category_id) ? $product->category_id : ''}}" > -->
     {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="long_description" style="font-size:18px;">{{ 'Long Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="long_description" type="textarea" id="long_description" >{{ isset($product->long_description) ? $product->long_description : ''}}</textarea>
     {!! $errors->first('long_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="tip_size" style="font-size:18px;">{{ 'Tip Size' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="tip_size" type="text" id="tip_size" value="{{ isset($product->tip_size) ? $product->tip_size : ''}}" >
     {!! $errors->first('tip_size', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="key_features" style="font-size:18px;">{{ 'Key Features' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="key_features" type="textarea" id="key_features" >{{ isset($product->key_features) ? $product->key_features : ''}}</textarea>
     {!! $errors->first('key_features', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="front_image" style="font-size:18px;">{{ 'Front Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="front_image" type="file" id="front_image" value="{{ isset($product->front_image) ? $product->front_image : ''}}" >
     {!! $errors->first('front_image', '<p class="help-block">:message</p>') !!}
      </div>
 </div><br/>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1":"Active","0":"Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($product->status) && $product->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
