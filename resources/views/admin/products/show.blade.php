@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Product  {{ $product->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_products', 'delete_products')
                         <a href="{{ url('/admin/products/' . $product->id . '/edit') }}" title="Edit Product"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/products' . '/' . $product->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Product" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $product->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $product->name }} </td></tr>
                                    <tr><th> Short Description </th><td> {{ $product->short_description }} </td></tr>
                                    <tr><th> Tag Line </th><td> {{ $product->tag_line }} </td></tr>
                                    <tr><th> Category Name </th><td> {{ $product->category_name }} </td></tr>
                                    <tr><th> Long Description </th><td> {{ $product->long_description }} </td></tr>
                                    <tr><th> Tip Size </th><td> {{ $product->tip_size }} </td></tr>
                                    <tr><th>Key Features</th><td>{{ $product->key_features}}</td></tr>
                                    <tr><th>Front Image</th><td><img width="50%" src="{{ asset('storage/'.$product->front_image)}}"></td></tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
