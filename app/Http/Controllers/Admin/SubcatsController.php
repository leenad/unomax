<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Subcat;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Models\Category;

class SubcatsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $subcats = Subcat::where('title', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('category_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $subcats = Subcat::latest()->paginate($perPage);
        }
       
        foreach ($subcats as $key => $value) {     
        //print_r($key);       
            $category_details = Category::find($value->category_id);
            //print_r($category_details->name);
            $subcats[$key]->category_name = $category_details->name;
        }
        //dd($subcats);
        //$categories =Category::all();
        return view('admin.subcats.index', compact('subcats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.subcats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required|max:10'
		]);
        $requestData = $request->all();
        
        Subcat::create($requestData);

        return redirect('admin/subcats')->with('flash_message', 'Subcat added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $subcat = Subcat::findOrFail($id);

        return view('admin.subcats.show', compact('subcat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $subcat = Subcat::findOrFail($id);

        return view('admin.subcats.edit', compact('subcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required|max:10'
		]);
        $requestData = $request->all();
        
        $subcat = Subcat::findOrFail($id);
        $subcat->update($requestData);

        return redirect('admin/subcats')->with('flash_message', 'Subcat updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Subcat::destroy($id);

        return redirect('admin/subcats')->with('flash_message', 'Subcat deleted!');
    }
}
