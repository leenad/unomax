<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Productimage;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Storage;
use App\Authorizable;
use App\Models\Category;
use App\Models\Product;
use App\Models\Color;

class ProductimagesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $productimages = Productimage::where('product_id', 'LIKE', "%$keyword%")
                ->orWhere('color_id', 'LIKE', "%$keyword%")
                ->orWhere('product_color_image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $productimages = Productimage::latest()->paginate($perPage);
        }
        foreach ($productimages as $key => $value) {     
        //print_r($key);       
            $product_details = Product::find($value->product_id);
            $product_category_details = Category::find($product_details->category_id);
            $color_details = Color::find($value->color_id);
            $productimages[$key]->product_name = $product_details->name. ' - '.$product_category_details->name;
            $productimages[$key]->color_name = $color_details->name;
        }

        return view('admin.productimages.index', compact('productimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        //$category = Category::all();
        $products = Product::all();
        foreach ($products as $key => $value) {     
        //print_r($key);       
            $category_details = Category::find($value->category_id);
            //print_r($category_details->name);
            $products[$key]->category_name = $category_details->name;
        }
        $color = Color::all();
        return view('admin.productimages.create',compact('products','color'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'product_id' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('product_color_image')) {
            $requestData['product_color_image'] = $request->file('product_color_image')
                ->store('uploads', 'public');
        }

        Productimage::create($requestData);

        return redirect('admin/productimages')->with('flash_message', 'Product Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $productimage = Productimage::findOrFail($id);
        //get product name
        $product_details = Product::find($productimage->product_id);
        $category_details = Category::find($product_details->category_id);
        $productimage->product_name = $product_details->name." - ".$category_details->name;
        //get color name
        $color_details = Color::find($productimage->color_id);
        $productimage->color_name = $color_details->name;

        return view('admin.productimages.show', compact('productimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $productimage = Productimage::findOrFail($id);
        $product_details = Product::find($productimage->product_id);
        $category_details = Category::find($product_details->category_id);
        $productimage->product_name = $product_details->name." - ".$category_details->name;
        
        $color_details = Color::find($productimage->color_id);
        $productimage->color_name = $color_details->name;
        $products = Product::all();
        $color = Color::all();

        return view('admin.productimages.edit', compact('productimage','products','color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'product_id' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('product_color_image')) {
            $requestData['product_color_image'] = $request->file('product_color_image')
                ->store('uploads', 'public');
        }

        $productimage = Productimage::findOrFail($id);
        $productimage->update($requestData);

        return redirect('admin/productimages')->with('flash_message', 'Productimage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Productimage::destroy($id);

        return redirect('admin/productimages')->with('flash_message', 'Productimage deleted!');
    }
}
